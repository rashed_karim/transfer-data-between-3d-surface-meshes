# README #

This program transfers the data between two meshes (source and target) by using the point correspondence information obtained form the transformation process. 

### Important notes ###

* Transform point scalars from source to target, using point correspondence information 
* Source and target were fused using [CartoFuse](https://www.doc.ic.ac.uk/~rkarim/mediawiki/index.php?title=Carto_fuse_v2a) software  
* The point correspondence information is obtained from Cartofuse software during the fusion process. 
* In the fusion process, It is important to save the 'Source transformed' polygon after the Landmark and ICP steps. 
* Version 1.0 

### Usage ###

The program expects all input file VTKs to contain **Point Data** scalars.

```
#!c++

post_fusion <source_poly_vtk> <target_poly_vtk> <source_transfomred_poly_vtk> <output_vtk>
```



### Who do I talk to? ###

* Rashed Karim, rashed.karim@kcl.ac.uk