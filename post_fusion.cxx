#define HAS_VTK 1
#define _IS_DEBUG 1

#include "vtkPointData.h"
#include <vtkPointPicker.h>
#include <vtkCommand.h>
#include <vtkMarchingCubes.h>
#include <vtkContourFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkPolyDataMapper.h>
#include <vtkCamera.h>
#include <vtkMarchingCubes.h>
#include <vtkVectorNorm.h>
#include <vtkDataSetMapper.h>
#include <vtkImageToPolyDataFilter.h>
#include <vtkPolyDataReader.h>
#include <vtkLookupTable.h>
#include <vtkSphereSource.h>
#include <vtkCallbackCommand.h>
#include <vtkProperty.h>
#include <vtkImagePlaneWidget.h>
#include <vtkImageActor.h>
#include <vtkSmartPointer.h>
#include <vtkCellArray.h>
#include <vtkPolyDataWriter.h>
#include <vtkCellData.h>
#include <vtkPolyDataReader.h>
#include <vtkIterativeClosestPointTransform.h>
#include <vtkLandmarkTransform.h>
#include <vtkMath.h>
#include <vtkMatrix4x4.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkMaskPoints.h>
#include <vtkFloatArray.h>
#include <vtkPointLocator.h>


#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;


  
/*
*   For each vertex in target, gets the closest vertex on the source post-transformation, and copies the point ID 
*   Writes the target polygon with Point IDs from source as output 
*/
void TransferPointID(char* source_poly_fn, char* target_poly_fn, char* source_poly_f_fn, char* output_poly_fn) 
{
  double xyz_target[3];
  int closestPointID; 

  vtkSmartPointer<vtkPolyData> source_poly =vtkSmartPointer<vtkPolyData>::New();  
  vtkSmartPointer<vtkPolyData> target_poly =vtkSmartPointer<vtkPolyData>::New();  
  vtkSmartPointer<vtkPolyData> source_t_poly =vtkSmartPointer<vtkPolyData>::New();  

  vtkSmartPointer<vtkPolyDataReader> reader1 = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader1->SetFileName(source_poly_f_fn); 
  reader1->Update();
  source_t_poly = reader1->GetOutput();

  vtkSmartPointer<vtkPolyDataReader> reader2 = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader2->SetFileName(source_poly_fn); 
  reader2->Update();
  source_poly = reader2->GetOutput();

  vtkSmartPointer<vtkPolyDataReader> reader3 = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader3->SetFileName(target_poly_fn); 
  reader3->Update();
  target_poly = reader3->GetOutput();

  vtkSmartPointer<vtkFloatArray> source_poly_scalars = vtkSmartPointer<vtkFloatArray>::New();
  source_poly_scalars = vtkFloatArray::SafeDownCast(source_poly->GetPointData()->GetScalars());  

  vtkSmartPointer<vtkFloatArray> target_poly_scalars = vtkSmartPointer<vtkFloatArray>::New();
  //target_poly_scalars = vtkFloatArray::SafeDownCast(target_poly->GetPointData()->GetScalars());
  
  // vtkPointLocator is a spatial search object to quickly locate points in 3D 
	// Quick search is possible by dividing a specified region of space into a regular array of rectangular buckets
	vtkSmartPointer<vtkPointLocator> point_locator = vtkSmartPointer<vtkPointLocator>::New();
	point_locator->SetDataSet(source_t_poly); 
	point_locator->AutomaticOn(); 
	point_locator->BuildLocator();
	
	//source_poly_scalars = vtkFloatArray::SafeDownCast(source_poly->GetPointData()->GetScalars());

	for (int i=0;i<target_poly->GetNumberOfPoints();i++)
	{
		target_poly->GetPoint(i, xyz_target); 
		closestPointID = -1;
	
		closestPointID = point_locator->FindClosestPoint(xyz_target); 

		if (closestPointID > -1) 
		{
      //cout << closestPointID << "\t" << source_poly_scalars->GetNumberOfTuples() << endl;

     
		  target_poly_scalars->InsertNextTuple1(source_poly_scalars->GetTuple1(closestPointID));	// point ID from nearest vertex on source polygon  
		}

	}

	target_poly->GetPointData()->SetScalars(target_poly_scalars);

  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetInputData(target_poly);
  writer->SetFileName(output_poly_fn);
  writer->Update();

}

int main(int argc, char **argv)
{
  
   
  
  char* poly_source_fn, *poly_target_fn, *poly_out_fn, *poly_source_transformed_fn;
   
  
  
  if (argc != 5)
  {
     cerr << "Not enough parameters\nUsage: <source.vtk> <target.vtk> <source_transformed.vtk> <output_vtk>"<< endl; 
     exit(1);
  }
  
  

  poly_source_fn = argv[1];
  poly_target_fn = argv[2];
  poly_source_transformed_fn = argv[3];
  poly_out_fn = argv[4];

  
	TransferPointID(poly_source_fn, poly_target_fn, poly_source_transformed_fn, poly_out_fn);
  
  
}